package xyz.fiberoptic.fiber.FairCloudBot.Config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class SaveProperties {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		String rootDir = System.getProperty("user.dir");
		File configFile = new File(rootDir + File.separator + "config.properties");
		
		if (!configFile.exists()) {
			Properties prop = new Properties();
			OutputStream output = null;

			try {

				output = new FileOutputStream("config.properties");

				// set the properties value
				prop.setProperty("BotName", "ExampleName");
				prop.setProperty("DiscordToken", "123456789");
				prop.setProperty("OwnerID", "987654321");
				prop.setProperty("MemePower", "text");
				prop.setProperty("ServerAddress", "localhost");
				prop.setProperty("ServerPort", "25565");
				prop.setProperty("MulticraftBanner", "www.banner-url-here.com");
				prop.setProperty("VacationRole", "123456789");

				// save properties to project root folder
				prop.store(output, null);

			} catch (IOException io) {
				io.printStackTrace();
			} finally {
				if (output != null) {
					try {
						output.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			LoadProperties.main(null);
		}
	}
}
