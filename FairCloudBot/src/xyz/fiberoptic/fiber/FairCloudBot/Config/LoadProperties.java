package xyz.fiberoptic.fiber.FairCloudBot.Config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import xyz.fiberoptic.fiber.FairCloudBot.FairCloudBot;
import xyz.fiberoptic.fiber.FairCloudBot.Utils.Utils;

public class LoadProperties {
	public static void main(String[] args) {

		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("config.properties");

			// load a properties file
			prop.load(input);
			
			FairCloudBot.botName = prop.getProperty("BotName");
			FairCloudBot.discordToken = prop.getProperty("DiscordToken");
			FairCloudBot.ownerID = prop.getProperty("OwnerID");
			FairCloudBot.memePower = prop.getProperty("MemePower");
			FairCloudBot.serverAddress = prop.getProperty("ServerAddress");
			
			if (Utils.isInt(prop.getProperty("ServerPort"))) {
				FairCloudBot.serverPort = Integer.valueOf(prop.getProperty("ServerPort"));
			} else {
				System.out.println("Oops, server port needs to be an integer.");
			}

			FairCloudBot.multicraftBanner = prop.getProperty("MulticraftBanner");
			FairCloudBot.vacationRole = prop.getProperty("VacationRole");
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
}
