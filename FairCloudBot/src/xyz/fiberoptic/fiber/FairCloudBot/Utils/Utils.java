package xyz.fiberoptic.fiber.FairCloudBot.Utils;

import java.util.Random;

import de.btobastian.javacord.entities.message.Message;
import xyz.fiberoptic.fiber.FairCloudBot.FairCloudBot;

public class Utils {    
    public static boolean isDouble(String s) {
    	try {
    		Double.parseDouble(s);
    		return true;
    	} catch (Exception e) {
    		return false;
    	}
    }
    
    public static boolean isInt(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }    
    
    public static int getRnd(int max) {
    	Random rand = new Random();
    	
    	int n = rand.nextInt(max) + 1;
    	return n;
    }
    
    public static boolean isAdmin(String authorID, Message message) {
    	if (!FairCloudBot.adminList.isEmpty()) {
    		if (FairCloudBot.adminList.contains(authorID)) {
        		return true;
        	} else {
    			if (FairCloudBot.memePower.equalsIgnoreCase("text")) {
    				// Text
    				message.reply("Hah! You have no power here!");
    			} else if (FairCloudBot.memePower.equalsIgnoreCase("image")) {
    				// "You have no power here!"
    				// image
    				message.reply("https://i.imgur.com/ODTJpZp.jpg");
    			} else if (FairCloudBot.memePower.equalsIgnoreCase("gif")) {
    				// "You have no power here!" gif    				
    				int n = getRnd(FairCloudBot.memeGifs.size());
    				String url = FairCloudBot.memeGifs.get(n);    				
    				message.reply(url);
    			} else {
    				message.reply("Hah! You have no power here!");
    			}
    			return false;
    		}
    	} else {
			message.reply("Error: AdminList is Empty! Contact fiber.");
			return false;
		}
    }
    
}
