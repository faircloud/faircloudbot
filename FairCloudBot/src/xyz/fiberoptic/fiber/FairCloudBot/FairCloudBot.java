package xyz.fiberoptic.fiber.FairCloudBot;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.Timer;

import com.google.common.util.concurrent.FutureCallback;

import de.btobastian.javacord.DiscordAPI;
import de.btobastian.javacord.Javacord;
import de.btobastian.javacord.entities.Channel;
import de.btobastian.javacord.entities.Server;
import de.btobastian.javacord.entities.User;
import de.btobastian.javacord.entities.message.Message;
import de.btobastian.javacord.entities.message.MessageHistory;
import de.btobastian.javacord.entities.permissions.Role;
import de.btobastian.javacord.listener.message.MessageCreateListener;
import xyz.fiberoptic.fiber.FairCloudBot.Config.SaveProperties;
import xyz.fiberoptic.fiber.FairCloudBot.Data.LoadData;
import xyz.fiberoptic.fiber.FairCloudBot.Data.SaveData;
import xyz.fiberoptic.fiber.FairCloudBot.Utils.Utils;

public class FairCloudBot {
	;

	// CONFIG OPTIONS
	public static String botName = null;
	public static String ownerID = null;
	public static String discordToken = null;
	public static String memePower = null;
	public static String serverAddress = null;
	public static int serverPort = 25565;
	public static String multicraftBanner = null;
	public static String vacationRole = null;

	// === ARRAYS === //
	public static ArrayList<String> regex = new ArrayList<String>();
	public static ArrayList<String> adminList = new ArrayList<String>();
	public static ArrayList<String> memeGifs = new ArrayList<String>();
	

	// === HASHMAPS === //
	public static HashMap<String, String> regexReply = new HashMap<String, String>();
	public static HashMap<String, DiscordAPI> discordHash = new HashMap<String, DiscordAPI>();
	public static HashMap<String, String> vacationList = new HashMap<String, String>();
	
	
	// === Timers === //
	public static Timer timer = new Timer(0, null);

	public static void main(String[] args) throws IOException {
		SaveProperties.main(null);
		LoadData.loadAdmins();
		LoadData.loadVacation();
		LoadData.loadGifs();
		
		if (ownerID != null) {
			if (!adminList.contains(ownerID)) {
				adminList.add(ownerID);
			}
		}

		if (discordToken != null) {
			iniBot(discordToken);
		}

		regex.add("\\bowner[s]?");
		regex.add("\\bapply");
		regex.add("\\bapplication");

		String ownerReply = "Hello there {AUTHORMENTION}! My name is {BOTNAME}, I just wanted to let you know that we don't like the term 'owner' around here. Instead, we use the title 'Head-Admin'.";
		String applyReply = "Hello there {AUTHORMENTION}! If you're interested in applying, please check out our forum post on MinecraftForums. More information can be found there. Or, someone here may help you out. Either way, please be patient, we'll get to you as soon as possible!";

		regexReply.put("\\bowner[s]?", ownerReply);
		regexReply.put("\\bapply", applyReply);
		regexReply.put("\\bapplication", applyReply);
		
		saveData();
		
	}

	
	public static void saveData() {		
		new Thread(new Runnable() {
		    public void run() {
		        try {
					SaveData.saveAdmins();
					SaveData.saveVacation();
					SaveData.saveGifs();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        
		        
		        timer = new Timer((1000 * 15), new ActionListener() {
		        	  @Override
		        	  public void actionPerformed(ActionEvent arg0) {
		        	    saveData();
		        	  }
		        	});
		        	timer.setRepeats(false); // Only execute once
		        	timer.start(); // Go go go!
		    }
		}).start();
	}
	
	
	public static void iniBot(String token) {
		DiscordAPI api = Javacord.getApi(token, true);
		discordHash.put(botName, api);
		startBot();
	}

	public static void startBot() {
		if (discordHash.containsKey(botName)) {
			DiscordAPI api = discordHash.get(botName);
			api.connect(new FutureCallback<DiscordAPI>() {
				@Override
				public void onSuccess(DiscordAPI api) {
					// register listener
					api.registerListener(new MessageCreateListener() {						
						public boolean sendMessage = false;
						public String regexMatch = "";

						@SuppressWarnings("unused")
						@Override
						public void onMessageCreate(DiscordAPI api, Message message) {
							if (!message.getAuthor().isBot() && !message.getAuthor().isYourself()) {
								if (!message.isPrivateMessage()) {
									String author = message.getAuthor().getName();
									String authorMention = message.getAuthor().getMentionTag();
									String authorId = message.getAuthor().getId();
									String msg = message.getContent().toLowerCase();
									String[] strings = message.getContent().split("\\s+");
									Server server = message.getChannelReceiver().getServer();
									Role vRole = server.getRoleById(vacationRole);

									// === REGEX BASED MESSAGES === //
									for (String s : regex) {
										final Pattern pattern = Pattern.compile(s, Pattern.CASE_INSENSITIVE);
										final Matcher matcher = pattern.matcher(msg);

										while (matcher.find()) {
											sendMessage = true;
											regexMatch = s;
										}

									}

									if (sendMessage) {
										message.reply(regexReply.get(regexMatch).replace("{AUTHORMENTION}", authorMention).replace("{BOTNAME}", botName));
										sendMessage = false;
										regexMatch = "";
									}

									// === REGEX BASED MESSAGES === //

									// === HELP MENU === //
									if (message.getContent().startsWith("!help")) {
										message.reply(""
												+ "**Available Commands: ** \n"
												+ "!server - *Displays the server status.* \n"
												+ "!vacation <on/off> - *This toggles you as 'on vacation'.* \n"
												+ "!meeting - *Displays how long until next meeting.* \n"
												+ "!purge - *Sets the date and time of next meeting.* \n"
												+ "!getid - *Get a users Discord ID.* \n"
												+ "!listadmin - *Lists admins for this bot.* \n"
												+ "!setmeeting - *Sets the date and time of next meeting.* \n"
												+ "!addgif - *This adds a gif to our collection of replies.* \n"
												+ "!delgif - *This deletes a gif from our collection of replies.* \n"
												+ "!addadmin - *Adds a user as an admin for this bot.* \n"
												+ "!deladmin - *Del a user as an admin for this bot.* \n"
												+ "!die - *This kills the robot.* \n"
												+ "!help - *This help menu.* \n"
												+ "info - *Information about the Author.*");
									}
									// === HELP MENU === //

									
									
									
									// === AUTHOR INFORMATION === //
									if (message.getContent().startsWith("!info")) {
										message.reply("" 
												+ "**Author Info: ** \n"
												+ "This bot was custom written by FiberOptic. If you've found any bugs, glitches, errors or typos, please forward messages to him. \n"
												+ "If you feel you need access to a command, or want to learn more about me, please contact Fiber.");
									}
									// === AUTHOR INFORMATION === //
									
									
									
									
									
									// === POST SERVER INFORMATION === //
									if (message.getContent().startsWith("!server")) {
										message.reply("**Server Status: ** \n " + multicraftBanner);
									}
									// === POST SERVER INFORMATION === //
									
									
									
									// === PURGE MSG === //
									if (message.getContent().startsWith("!purge")) {
										if (Utils.isAdmin(authorId, message)) {
											if (strings.length <= 1) {
												message.reply("Command: !purge <number>");
											}
											
											if (strings.length == 2) {
												if (Utils.isInt(strings[1])) {
													int x = Integer.valueOf(strings[1]);
													String chanID = message.getChannelReceiver().getId();
													Channel chan = server.getChannelById(chanID);
																										
													try {
														Future<MessageHistory> future = chan.getMessageHistory(x);
														ArrayList<String> list = new ArrayList<String>();
														
														for (Message m : future.get().getMessages()) {
															list.add(m.getId());
														}
														
														String[] array = list.toArray(new String[list.size()]);
														
														chan.bulkDelete(array);														
													} catch (InterruptedException e) {
														
													} catch (ExecutionException e) {
														// TODO Auto-generated catch block
														e.printStackTrace();
													}
												} else {
													message.reply("Command: !purge <number>");
												}
											}
											
											if (strings.length >= 3) {
												message.reply("Command: !purge <number>");
											}
										}
									}
									// === PURGE MSG === //
									
									
									
									// === ADD GIF === //
									if (message.getContent().startsWith("!addgif")) {
										if (Utils.isAdmin(authorId, message)) {
											if (strings.length <= 1) {
												message.reply("Command: !addgif <link>");
											}
											
											if (strings.length == 2) {
												memeGifs.add(strings[1]);
												message.reply("GIF has been added.");
												try {
													SaveData.saveGifs();
												} catch (FileNotFoundException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
											}
											
											if (strings.length >= 3) {
												message.reply("Command: !addgif <link>");
											}
										}
									}
									// === ADD GIF === //
									
									
									// === DEL GIF === //
									if (message.getContent().startsWith("!delgif")) {
										if (Utils.isAdmin(authorId, message)) {
											String url = strings[1];
																					
											if (strings.length <= 1) {
												message.reply("Command: !delgif <link>");
											}
											
											if (strings.length == 2) {
												if (memeGifs.contains(url)) {
													for (int i = 0; i < memeGifs.size(); i++) {
														if (memeGifs.get(i).equals(url)) {
															memeGifs.remove(i);
															message.reply("GIF has been removed.");
														}
													}
												} else {
													message.reply("Error: GIF Not found.");
												}
												
												try {
													SaveData.saveGifs();
												} catch (FileNotFoundException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
											}
											
											if (strings.length >= 3) {
												message.reply("Command: !delgif <link>");
											}										
										}
									}
									// === DEL GIF === //
									
									
									// === VACATION MODE === //
									if (message.getContent().startsWith("!vacation")) {
										if (strings.length <= 1) {
											message.reply("Command: !vacation <on/off>");
										}
										
										if (strings.length == 2) {
											String flag = strings[1];
											
											if (flag.equalsIgnoreCase("on") || flag.equalsIgnoreCase("off")) {
												User user = message.getAuthor();	
												
												if (flag.equalsIgnoreCase("on")) {
													message.reply(user.getMentionTag() + " has gone on Vacation!");
																																								
													int max = 0;
													String role = null;
													
													for (Role r : user.getRoles(server)) {
														int pos = r.getPosition();
														
														r.removeUser(user);
														
														if (pos > max) {
															max = pos;
															role = r.getId();
														}
													}
													
													vacationList.put(user.getId(), role);
																										
													new Thread(new Runnable() {
													    public void run() {
													        timer = new Timer((1000 * 1), new ActionListener() {
													        	  @Override
													        	  public void actionPerformed(ActionEvent arg0) {
													        		  vRole.addUser(user);
													        		  try {
																		SaveData.saveVacation();
																	} catch (IOException e) {
																		// TODO Auto-generated catch block
																		e.printStackTrace();
																	}
													        	  }
													        	});
													        	timer.setRepeats(false); // Only execute once
													        	timer.start(); // Go go go!
													    }
													}).start();
												}
												
												if (flag.equalsIgnoreCase("off")) {
													if (vacationList.containsKey(user.getId())) {
														
														String rank = vacationList.get(user.getId());
														Role oldRank = server.getRoleById(rank);
														
														for (Role r : user.getRoles(server)) {																
															r.removeUser(user);
														}
														
														new Thread(new Runnable() {
														    public void run() {
														        timer = new Timer((1000 * 1), new ActionListener() {
														        	  @Override
														        	  public void actionPerformed(ActionEvent arg0) {
														        		  oldRank.addUser(user);
														        		  try {
																			SaveData.saveVacation();
																		} catch (IOException e) {
																			// TODO Auto-generated catch block
																			e.printStackTrace();
																		}
														        	  }
														        	});
														        	timer.setRepeats(false); // Only execute once
														        	timer.start(); // Go go go!
														    }
														}).start();
														
														vacationList.remove(user.getId());
														message.reply(user.getMentionTag() + " has returned from Vacation!");
														
													} else {
														message.reply("You're not currently set on vacation. If this is an error, contact Fiber.");
													}
												}												
											} else {
												message.reply("Command: !vacation <on/off>");
											}											
										}
										
										if (strings.length >= 3) {
											message.reply("Command: !vacation <on/off>");
										}
									}
									// === VACATION MODE === //
									
									
									// === GET USERS ID === //
									if (message.getContent().startsWith("!getid")) {
										if (Utils.isAdmin(authorId, message)) {
											if (strings.length <= 1) {
												message.reply("Command: !getid <@Username>");
											}

											if (strings.length == 2) {
												for (User user : message.getMentions()) {
													message.reply(user.getName() + "'s Discord ID is: " + user.getId());
													break;
												}
											}

											if (strings.length >= 3) {
												message.reply("Command: !getid <@Username>");
											}
										}
									}
									// === GET USERS ID === //
									
									
									
									// === GET ROLES === //
									if (message.getContent().equals("!getroles")) {
										if (Utils.isAdmin(authorId, message)) {
											for (Role r : server.getRoles()) {
												message.reply("**Names**: " + r.getMentionTag() + " - **ID**: " + r.getId());
											}
										}
									}
									// === GET ROLES === //
									
									
									

									// === ADD ADMIN === //
									if (message.getContent().startsWith("!addadmin")) {										
										if (Utils.isAdmin(authorId, message)) {
											if (strings.length <= 1) {
												message.reply("!addadmin <@Username>");
											}

											if (strings.length == 2) {
												for (User user : message.getMentions()) {
													if (adminList.contains(user.getId())) {
														message.reply("Error: That user is already an admin.");
														break;
													} else {
														message.reply(user.getName() + " has been added to the Admin List.");
														adminList.add(user.getId());
														break;
													}
												}
											}

											if (strings.length >= 3) {
												message.reply("!addadmin <@Username>");
											}
										}
									}
									// === ADD ADMIN === //
									
									
									
									
									
	                                // === DEL ADMIN === //
	                                if (message.getContent().startsWith("!deladmin")) {
	                                	if (Utils.isAdmin(authorId, message)) {
	                                		if (strings.length <= 1) {
	                                			message.reply("!deladmin <@Username>");
	                                    	}
	                                		
	                                		if (strings.length == 2) {
	                                			
	                                			boolean found = false;
	                                			
	                                			for (User user : message.getMentions()) {
	                                				if (ownerID.equals(user.getId())) {
	                                					message.reply("Error: That user is protected and can not be removed.");
	                                				} else {
	                                					if (adminList.size() >= 2) {
	                                            			for (int i = 0; i < adminList.size(); i++) {
	                                            				if (adminList.get(i).equals(user.getId())) {
	                                            					adminList.remove(i);
	                                            					message.reply(user.getName() + " has been deleted from the Admin List.");
	                                            					found = true;
	                                            					break;
	                                            				}
	                                            			}
	                                    				} else {
	                                    					message.reply("Error: the AdminList must contain at least one user.");
	                                    				}
	                                				}
	                                    		}
	                                			
	                                			if (!found) {
	                                				message.reply("Error: " + message.getMentions().get(0) + " was not found in the AdminList.");
	                                			}
	                                    	}
	                                		
	                                		if (strings.length >= 3) {
	                                			message.reply("!deladmin <@Username>");
	                                    	}
										}
	                                }
	                                // === DEL ADMIN === //
									
									
									
	                                // === LIST ADMIN === //
	                                if (message.getContent().equals("!listadmin") || message.getContent().equals("!listadmins") || message.getContent().equals("!adminlist")) {
	                                	if (!adminList.isEmpty()) {
	                                		
	                                		message.reply("**Admin List:** \n");
	                                		
	                                		try {
												TimeUnit.SECONDS.sleep(1);
											} catch (InterruptedException e) {
												// TODO Auto-generated catch
												// block
											}
	                                		
	                                		for (int i = 0; i < adminList.size(); i++) {
	                                			for (User user : api.getUsers()) {
	                                				if (user.getId().equals(adminList.get(i))) {
	                                					if (i == adminList.size()) {
	                                        				message.reply(" [+] *" + user.getName() + "*");
	                                        			} else {
	                                        				message.reply(" [+] *" + user.getName() + "* \n");
	                                        			}
	                                				}
	                                			}
	                            			}
	                                	} else {
	                                		message.reply("Error: AdminList is Empty! Contact fiber.");
	                                	}
	                                }
	                                // === LIST ADMIN === //
									
									

									// === KILL BOT === //
									if (message.getContent().equals("!die")) {
										if (Utils.isAdmin(authorId, message)) {
											message.reply("Fine, I didn't want to be here anyway. Bye!");
											try {
												SaveData.saveAdmins();
												SaveData.saveVacation();
												SaveData.saveGifs();
											} catch (FileNotFoundException e1) {
												// TODO Auto-generated catch block
												e1.printStackTrace();
											} catch (IOException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
											try {
												TimeUnit.SECONDS.sleep(1);
											} catch (InterruptedException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
											stopBot();
										}
									}
									// === KILL BOT === //
									
									
									
									// === DEBUG === //
									if (message.getContent().equals("!debug")) {
										if (Utils.isAdmin(authorId, message)) {
											for (String s : adminList) {
												message.reply("String: " + s);
											}
										}
									}
									// === DEBUG === //
									
									
									
									
								} else {
									// PRIVATE MESSAGES
								}
							}
						}
					});
				}

				@Override
				public void onFailure(Throwable t) {
					t.printStackTrace();
				}
			});
		} else {
			System.out.println("Error: Bot " + botName + " not found.");
		}
	}

	public static void stopBot() {
		if (discordHash.containsKey(botName)) {
			DiscordAPI api = discordHash.get(botName);
			timer.stop();
			api.disconnect();
			System.exit(0);
		} else {
			System.out.println("Error: Bot " + botName + " not found.");
		}
	}

}
