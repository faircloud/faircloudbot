package xyz.fiberoptic.fiber.FairCloudBot.Data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Properties;

import xyz.fiberoptic.fiber.FairCloudBot.FairCloudBot;

public class SaveData {
	
	public static void saveAdmins() throws FileNotFoundException {
		String fileName = "adminlist.txt";
		String rootDir = System.getProperty("user.dir");
		File file = new File(rootDir + File.separator + fileName);
		
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (!FairCloudBot.adminList.isEmpty()) {
			
			try {
				if (file.exists()) {
					file.delete();
					file.createNewFile();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			PrintWriter pw = new PrintWriter(new FileOutputStream(file));
			for (String s : FairCloudBot.adminList) {
				pw.println(s);
			}
			pw.close();
		}
	}
	
	
	public static void saveVacation() throws IOException {
		String fileName = "vacationlist.txt";
		String rootDir = System.getProperty("user.dir");
		File file = new File(rootDir + File.separator + fileName);
		
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if (!FairCloudBot.vacationList.isEmpty()) {
			
			try {
				if (file.exists()) {
					file.delete();
					file.createNewFile();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			Properties properties = new Properties();
			for (Map.Entry<String,String> entry : FairCloudBot.vacationList.entrySet()) {
				properties.put(entry.getKey(), entry.getValue());
			}
			
			properties.store(new FileOutputStream(file), null);
		}
	}
	
	public static void saveGifs() throws FileNotFoundException {
		String fileName = "memeGifs.txt";
		String rootDir = System.getProperty("user.dir");
		File file = new File(rootDir + File.separator + fileName);
		
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (!FairCloudBot.memeGifs.isEmpty()) {
			
			try {
				if (file.exists()) {
					file.delete();
					file.createNewFile();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			PrintWriter pw = new PrintWriter(new FileOutputStream(file));
			for (String s : FairCloudBot.memeGifs) {
				pw.println(s);
			}
			pw.close();
		}
	}
}
