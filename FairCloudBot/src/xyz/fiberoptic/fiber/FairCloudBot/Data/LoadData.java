package xyz.fiberoptic.fiber.FairCloudBot.Data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import xyz.fiberoptic.fiber.FairCloudBot.FairCloudBot;

public class LoadData {

	public static void loadAdmins() throws IOException {
		FairCloudBot.adminList.clear();
		
		String fileName = "adminlist.txt";
		String rootDir = System.getProperty("user.dir");
		File file = new File(rootDir + File.separator + fileName);
		
		if (file.exists()) {
			FileInputStream fis = new FileInputStream(file);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			
			String line = null;
			while ((line = br.readLine()) != null) {
				FairCloudBot.adminList.add(line);
			}
			
			br.close();
		}
	}
	
	public static void loadVacation() throws IOException {
		FairCloudBot.vacationList.clear();
		
		String fileName = "vacationlist.txt";
		String rootDir = System.getProperty("user.dir");
		File file = new File(rootDir + File.separator + fileName);
		
		if (file.exists()) {
			Properties properties = new Properties();
			properties.load(new FileInputStream(file));

			for (String key : properties.stringPropertyNames()) {
			   FairCloudBot.vacationList.put(key, properties.get(key).toString());
			}
		}
	}
	
	public static void loadGifs() throws IOException {
		FairCloudBot.memeGifs.clear();
		
		String fileName = "memeGifs.txt";
		String rootDir = System.getProperty("user.dir");
		File file = new File(rootDir + File.separator + fileName);
		
		if (file.exists()) {
			FileInputStream fis = new FileInputStream(file);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			
			String line = null;
			while ((line = br.readLine()) != null) {
				FairCloudBot.memeGifs.add(line);
			}
			
			br.close();
		}
	}
	
}
